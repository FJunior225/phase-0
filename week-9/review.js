// Pseudo Code:
// Create a new list
	// create an empty hash
	// output empty hash
// Add an item with a quantity to the list
	// using dot notation, add item to grocery list with quantity of 1
	// output updated hash
// Remove an item from the list
	// delete a specific item from the hash (delete)
	// output the updated hash
// Update quantities for items in your list
	// adjust value of keys using dot notation
	// output updated list
// Print the list (Consider how to make it look nice!)

// Initial Solution
// Create a new list
	// create an empty hash
	// output empty hash
// var groceryList = {}
// // console.log(groceryList)

// // Add an item with a quantity to the list
// 	// using dot notation, add item to grocery list with quantity of 1
// 	// output updated hash
// groceryList.carrots = 12;
// groceryList.apples = 5;
// groceryList.cereal = {}
// groceryList.cereal["Cinnamon Toast Crunch"] = 1
// groceryList.cereal["Captain Crunch"] = 1
// groceryList.cereal["Frosted Mini Wheats"] = 1
// groceryList.vegetables = {}
// groceryList.vegetables.carrots = 12
// groceryList.vegetables.kale = "6 oz"
// groceryList.vegetables.spinach = "6 oz"
// console.log(groceryList)


// // Remove an item from the list
// 	// delete a specific item from the hash (delete)
// 	// output the updated hash
// delete groceryList.carrots
// console.log(groceryList)

// // Update quantities for items in your list
// 	// adjust value of keys using dot notation
// 	// output updated list
// groceryList.vegetables.kale = "3 oz"
// groceryList.cereal["Frosted Mini Wheats"] = 2
// console.log(groceryList)

// Refactored Solution
// try to use constructor function to look cleaner and better my understanding

function GroceryList(list) {
  this.list = {};
  for (var i = 0; i < list.length; i++) {
    this.list[list[i]] = 1;
  };
  
  this.add_item = function(item, quantity) {
    if (this.list.hasOwnProperty(item)) {
      this.list[item] += quantity;
    } else {
      this.list[item] = quantity;
    };
  };
  
  this.remove_item = function(item) {
    delete this.list[item];
  };
  
  this.update_item = function(item, quantity) {
    if (this.list.hasOwnProperty(item)) {
      this.list[item] = quantity;
    };
  };
  
  this.display_list = function() {
    console.log("-------------")
    console.log("My Grocery List")
    console.log("-------------")
    for (var item in this.list) {
      console.log(this.list[item] + ", " + item);
    };
  };
}


var groceryList = new GroceryList(["cereal","apples","carrots", "kale", "spinach"])
groceryList.add_item("chocolate", 5)
groceryList.display_list()
groceryList.remove_item("kale")
groceryList.display_list()
groceryList.update_item("cereal",3)
groceryList.display_list()
groceryList.add_item("oreos",16)
groceryList.display_list()

/*
What concepts did you solidify in working on this challenge? (reviewing the passing of information, objects, constructors, etc.)
Constructor functions, accessing nested structures and creating nested structures. My question would be with my refactored
solution is, how would you make my grocery list with categories such as vegetables and cereal with items under those 
properties? (like how I did with my initial solution)

What was the most difficult part of this challenge?
I used my Ruby solution as a reference, so the hardest part was matching up the Ruby built-in methods with JS built-in methods.
It took me a while to remember/look-up #hasOwnProperty - I had a hard time with that one.

Did an array or object make more sense to use and why?
An object made more sense to me because with a grocery list you want to keep track of items and their quantities.
Therefore, an object made more sense since arrays numeric values would only be their indices.


*/