// U3.W9:JQuery


// I worked on this challenge [by myself, with: ].
// This challenge took me [#] hours.

$(document).ready(function(){

//RELEASE 0:
  //link the image

//RELEASE 1:

  //Link this script and the jQuery library to the jQuery_example.html file and analyze what this code does.

$('body').css({'background-color': 'pink'})


//RELEASE 2:
  //Add code here to select elements of the DOM
  bodyElement = $('body')
  divElement = $('div')
  h1Element = $('h1')
  imgElement = $('img')

//RELEASE 3:
  // Add code here to modify the css and html of DOM elements
$('body > h1').css({'color': 'blue'})
$('body > h1').css({'border': 'solid black thick'})
$('body > h1').css({'visibility': 'fadeIn'})
$('div > h1').html('Sea Lions')
$('div > h1').css({'color': 'green'})

//RELEASE 4: Event Listener
  // Add the code for the event listener here
$('img').on('mouseover', function(e){
    e.preventDefault()
    $(this).attr('src', 'http://www.sealion-world.com/wp-content/uploads/Galapagos_sealion_pup.jpg')
})
$('img').click('mouseover', function(e){
    e.preventDefault()
    $(this).attr('src', 'dbc_logo.png')
})


//RELEASE 5: Experiment on your own
$('img').animate({'height':'400px', 'width':'500px'})






})  // end of the document.ready function: do not remove or write DOM manipulation below this.

/*
What is jQuery?
jQuery is a fast, small, and feature-rich JavaScript library. It makes things like HTML document traversal 
and manipulation, event handling, animation, and Ajax much simpler with an easy-to-use API that works across 
a multitude of browsers.

What does jQuery do for you?
It allows us to use JavaScript more easily with HTML docs. We can now make our websites we've created more
interactive with our audience to show off our skills as web developers.

What did you learn about the DOM while working on this challenge?
I learned how easy it is to sift through elements of your HTML page with the DOM. You can easily see objects
you've created just by typing in the element name (i.e. bodyElement = $('body').
)

*/