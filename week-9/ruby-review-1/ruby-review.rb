# Numbers to English Words


# I worked on this challenge with: Pat Skelly.
# This challenge took me [1.25] hours.


# Pseudocode
# coding numbers into words there are cases we will account for up to 100 
# the base case is numbers zero through nine. 
# special cases will be for teens those are unique 
# numbers in the 10s after the teens we will add the ones onto these
      # grab twenty english word val from complex_num
      # find out what the number is passed_number - 20 = single digit 
      # includes key basic num hash if so return val

# Initial Solution
def in_words(passed_number)
  
  basic_num = {1 => "one", 2 => "two", 3 => "three", 4 => "four", 
  5 => "five", 6 => "six", 7 => "seven", 8 => "eight", 9 => "nine", 
  10 => "ten", 11 => "eleven", 12 => "twelve", 13 => "thirteen", 
  14 => "fourteen", 15 => "fifteen", 16 => "sixteen", 17 => "seventeen", 
  18 => "eighteen", 19 => "nineteen"}
  
  complex_num = {2 => "twenty", 3 => "thirty", 4 => "fourty", 5 => "fifty", 
  6 => "sixty", 7 => "seventy", 8 => "eighty", 9 => "ninety", 
  100 => "hundred"}

 case 
    when passed_number < 20
      basic_num.each do |number, string|
        if number == passed_number
          return string
      	end  
      end
    when passed_number >= 20 && passed_number < 100 
    complex_num[(passed_number / 10).floor] + " " + basic_num[passed_number % 10]
    when passed_number = 100
      return "one hundred"
  end
    



# Refactored Solution

def in_words(passed_number)
  
  basic_num = {1 => "one", 2 => "two", 3 => "three", 4 => "four", 
  5 => "five", 6 => "six", 7 => "seven", 8 => "eight", 9 => "nine", 
  10 => "ten", 11 => "eleven", 12 => "twelve", 13 => "thirteen", 
  14 => "fourteen", 15 => "fifteen", 16 => "sixteen", 17 => "seventeen", 
  18 => "eighteen", 19 => "nineteen"}
  
  complex_num = {2 => "twenty", 3 => "thirty", 4 => "fourty", 5 => "fifty", 
  6 => "sixty", 7 => "seventy", 8 => "eighty", 9 => "ninety", 
  100 => "hundred"}

  case 
    when passed_number < 20
      if basic_num.has_key?(passed_number) 
          return basic_num[passed_number]  
      end
    when passed_number >= 20 && passed_number < 100 
    complex_num[(passed_number / 10).floor] + " " + basic_num[passed_number % 10]
    when passed_number = 100
      return "one hundred"
  end
end

p in_words(7)
p in_words(23)
p in_words(89)
p in_words(48)
p in_words(100)



# Reflection
# What concepts did you review in this challenge?
# We reviewed the concepts of a hash, iterating through a hash, accessing
# values within a hash and appending values from one hash to another.

# What is still confusing to you about Ruby?
# Refactoring and becoming more dynamic than our refactored solution. 
# Refactoring is going to be a huge huge concept in becoming successful in
# coding and if I can understand more built-in methods, than I can be more
# successful in refactoring. 

# What are you going to study to get more prepared for Phase 1?
# Just keep practicing tutorials and research anything I find confusing or hard
# to comprehend. I just need to keep gaining knowledge each and every day, learn from
# my peers and guides that will be teaching me and try not to be ashamed or afraid
# to ask for help.