// Eloquent JavaScript

// Run this file in your terminal using `node my_solution.js`. Make sure it works before moving on!

// Program Structure
// Write your own variable and do something to it.
var name = "FJ";
console.log(name);

// Write a short program that asks for a user to input their favorite food. 
// After they hit return, have the program respond with "Hey! That's my favorite too!" 
// (You will probably need to run this in the Chrome console (Links to an external site.) 
// rather than node since node does not support prompt or alert). 
// Paste your program into the eloquent.js file.
prompt ("What is your favorite food?"); 
	alert("That is my favorite too!");


// Complete one of the exercises: Looping a Triangle, FizzBuzz, or Chess Board
var triangle = "#";
while (triangle.length <= 7) {
  console.log(triangle)
  triangle += "#";  
}

// // Needed to look at hints to help figure out.
for (var n = 1; n <= 100; n += 1) {
  var output = "";
  if (n % 3 == 0)
    output += "Fizz";
  if (n % 5 == 0)
    output += "Buzz";
  console.log(output || n);
}
// // Functions

// Complete the `minimum` exercise.
function min(numOne, numTwo) {
  if (numOne < numTwo)
    return numOne;
  else
    return numTwo;
}


// Data Structures: Objects and Arrays
// Create an object called "me" that stores your name, age, 3 favorite foods, and a quirk below.
var me = {
	name: "FJ",
	age: 28,
	favoriteFood: ["Pizza", "Chocolate", "Pasta"],
	quirk: "I am secretly a Jedi master!"
}


