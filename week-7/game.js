 // Design Basic Game Solo Challenge

// This is a solo challenge

// Your mission description:
// Overall mission: Answer questions correctly to move to next position
// Goals: Avoid wrong answers that push you back a spot & get to end of board
// Characters: player 1 & 2 - Can add more after we get it working (hopefully 4)
// Objects: player(position), questions to answer, seconds to drink/give out, points
// Functions: move forward, move backwards, bonus? (move forward two spots
// or start from beginning)

// Pseudocode
// Declare a 'player'
// Add different functions to player for position movements (forwards & backwards)
// Move the player forwards if answers question correctly
// Move player backwards if answers question incorrectly
// Check player position throughout each round
// If player reaches end, player has won, game over!
// questions may need to be provided ahead of time and reused.

// Initial Code

// var players = []
// players.playerOne = {}
// players.playerOne.points = 0
// players.playerOne.posX = 0
// players.playerOne.posY = 0
// players.playerOne.success = false
// players.playerTwo = {}
// players.playerTwo.points = 0
// players.playerTwo.posX = 0
// players.playerTwo.posY = 5
// players.playerTwo.success = false

// // define a set of questions to be used randomly
// var riddles = [
//   {
//     riddle: "What falls but never breaks, and breaks but never falls?",
//     correctAnswer: "Day and Night"
//   },
//   {
//     riddle: "What is greater than God, more evil than the devil, the poor have it, the rich need it, and if you eat it, you'll die?",
//     correctAnswer: "Nothing"
//   },
//   {
//     riddle: "If you look at the number on my face you won't find thirteen anyplace.",
//     correctAnswer: "A clock"
//   },
//   {
//     riddle: "Tear one off and scratch my head what was red is black instead.",
//     correctAnswer: "A matchstick"
//   },
//   {
//     riddle: "The eight of us go forth not back to protect our king from a foes attack.",
//     correctAnswer: "Chess pawns"
//   },
//   {
//     riddle: "What room can no one enter?",
//     correctAnswer: "A mushroom"
//   },
//   {
//     riddle: "What is it that's always coming but never arrives?",
//     correctAnswer: "Tomorrow"
//   },
//   {
//     riddle: "The person who makes it, has no need of it. The person who buys it, has no use for it. The person who uses it can neither see nor feel it. What is it?",
//     correctAnswer: "A coffin"
//   },
//   {
//     riddle: "What can travel around the world while staying in a corner?",
//     correctAnswer: "A stamp"
//   },
//   {
//     riddle: "If you have me, you want to share me. If you share me, you haven't got me. What am I?",
//     correctAnswer: "A secret"
//   }
// ];

// function playGame(questions, participants) {
//  for(var i=0; i<questions.length; i++) {
//    console.log(questions[i].riddle);
//    // var answer = prompt("Please enter your answer");
   
//    for(var j=0; j<participants.length; j++) {  
//      if (answer === questions[i].correctAnswer) {
//        participants[j].score += 5
//      } else {
//        participants[j].score -= 3
//      }
//    }
//  }
// };

// playGame(riddles, players)

// // whoever has more points at the end of a round, moves forward
// function movement(players) {
//   if (players.playerOne.points > players.playerTwo.points) {
//     players.playerOne.posX += 10
//   } else if (players.playerOne.points < players.playerTwo.points) {
//     players.playerTwo.posX += 10
//   }
//   else (players.playerOne.points == players.playerTwo.points) 
//     players.playerOne.posX += 10
//     players.playerTwo.posX += 10
//   }
//   // At the end of each round, display players positions
//   console.log("Player One's position is " + players.playerOne.posX + "," + players.playerOne.posY);
//   console.log("Player Two's position is " + players.playerTwo.posX + "," + players.playerTwo.posY);
  
// // When a player reaches a specific point on the board, they win
// function success(players) {
//   if (players.playerOne.posX === 40 && players.playerTwo.posX != 40) {
//     players.playerOne.success = true;
//     console.log("Congratulations, you beat Player Two, you win!")
//   }
//   else if (players.playerTwo.posX === 40 && players.playerOne.posX != 40) {
//     players.playerTwo.success = true;
//     console.log("Congratulations, you beat Player One, you win!")  
//   }
//   else (players.playerOne.posX === 40 && players.playerTwo.posX === 40) 
//     players.playerTwo.success = true;
//     players.playerOne.success = true;
//     console.log("Wow, what a game! You both win!")
//   };



// Refactored Code

var players = []
players.playerOne = {}
players.playerOne.points = 0
players.playerOne.posX = 0
players.playerOne.posY = 0
players.playerOne.success = false
players.playerTwo = {}
players.playerTwo.points = 0
players.playerTwo.posX = 0
players.playerTwo.posY = 5
players.playerTwo.success = false

// define a set of questions to be used randomly
var riddles = [
  {
    riddle: "What falls but never breaks, and breaks but never falls?",
    correctAnswer: "Day and Night"
  },
  {
    riddle: "What is greater than God, more evil than the devil, the poor have it, the rich need it, and if you eat it, you'll die?",
    correctAnswer: "Nothing"
  },
  {
    riddle: "If you look at the number on my face you won't find thirteen anyplace.",
    correctAnswer: "A clock"
  },
  {
    riddle: "Tear one off and scratch my head what was red is black instead.",
    correctAnswer: "A matchstick"
  },
  {
    riddle: "The eight of us go forth not back to protect our king from a foes attack.",
    correctAnswer: "Chess pawns"
  },
  {
    riddle: "What room can no one enter?",
    correctAnswer: "A mushroom"
  },
  {
    riddle: "What is it that's always coming but never arrives?",
    correctAnswer: "Tomorrow"
  },
  {
    riddle: "The person who makes it, has no need of it. The person who buys it, has no use for it. The person who uses it can neither see nor feel it. What is it?",
    correctAnswer: "A coffin"
  },
  {
    riddle: "What can travel around the world while staying in a corner?",
    correctAnswer: "A stamp"
  },
  {
    riddle: "If you have me, you want to share me. If you share me, you haven't got me. What am I?",
    correctAnswer: "A secret"
  }
];

function playGame(questions, participants) {
 for(var i=0; i<questions.length; i++) {
   console.log(questions[i].riddle);
   // var answer = prompt("Please enter your answer");
   
   for(var j=0; j<participants.length; j++) {  
     if (answer === questions[i].correctAnswer) {
       participants[j].points += 5
     } else {
       participants[j].points -= 3
     }
   }
 }
};

// When a player reaches a specific point on the board, they win
function success(players) {
  if (players.playerOne.points > players.playerTwo.points) {
    players.playerOne.success = true;
    console.log("Congratulations, you beat Player Two, you win!")
  } else if (players.playerTwo.points > players.playerOne.points) {
    players.playerTwo.success = true;
    console.log("Congratulations, you beat Player One, you win!")  
  } else {
    players.playerTwo.success = true;
    players.playerOne.success = true;
    console.log("Wow, what a game! You both win!")
  };
};


playGame(riddles,players)
success(players)

// I left this section commented out so that I can add different features to the game as I
// continue my journey through DBC!

// players.playerOne.name = "FJ"
// players.playerTwo.name = "Ty"

// var setplayers = function(players) {
//   // returns and arry of the players names
// }

// var gameBoard = {
//   playerOne: ["", "", "", playerOne.name, "", "", "", "", "", ""],
//   playerTwo: [playerTwo.name, "", "", "", "", "", ""]
// }


// function movement(participants) {
//   if (participants.playerOne.points > participants.playerTwo.points) {
//     participants.playerOne.posX += 10
//   }
//   else if (participants.playerOne.points < participants.playerTwo.points) {
//     participants.playerTwo.posX += 10
//   }
//   else 
//     participants.playerOne.posX += 10
//     participants.playerTwo.posX += 10
//   };
// // At the end of each round, display players positions
//   console.log("Player One's position is " + participants.playerOne.posX + "," + participants.playerOne.posY);
//   console.log("Player Two's position is " + participants.playerTwo.posX + "," + participants.playerTwo.posY);




/* Reflection
What was the most difficult part of this challenge?
The most difficult part for me was getting different functions to work together - I was trying to understand the concept of
a constructor function when I did not necessarily need one ti implement my game correctly. 

What did you learn about creating objects and functions that interact with one another?
It was cool to see how the game works with different objects and their scope. I was able to create two objects and have them
interact with each other.

Did you learn about any new built-in methods you could use in your refactored solution? If so, what were they and how do they work?
It wasn't about built-in methods for me, instead I focused here on looping through different properties of objects and solidifying
my understanding of looping through nested data structures in JS.

How can you access and manipulate properties of objects?
In my code, I used dot notation to manipulate and access properties of objects. Within my players object, the winner is determined
by who has more points and to access those specific properties I used "players.playerOne.points."
*/