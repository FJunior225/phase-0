// Separate Numbers with Commas in JavaScript **Pairing Challenge**


// I worked on this challenge with Alyssa Page.

// Pseudocode
// input: integer
// output: string with commas
// steps to take:
// define a method that takes in an integer as as argument
// convert number to a string
// store string in a variable
// reverse the string
// every third place
//   push ',' into string
// return string with commas in correct place


// Initial Solution

// function separateComma(number) {
//   var numberString = number.toString();
//   var numberRev = numberString.split('').reverse().join('');
//   var counter = 0
//   var withComma = ''
  
//   for (var i=0; i < numberRev.length; i ++) {
//      withComma += numberRev[i];
//      counter ++;
//      if (counter % 3 == 0) { withComma += ','; }
       
//   }
  
//   withComma = withComma.split('').reverse().join('');
//   return withComma;
// }


// Refactored Solution

function separateComma(number) {
  var numberRev = number.toString().split('').reverse().join('');
  var counter = 0
  var withComma = ''
  
  for (var i=0; i < numberRev.length; i ++) {
    if (counter > 0 && counter % 3 == 0) { withComma += ','; } 
    withComma += numberRev[i];
    counter ++;
  }
  withComma = withComma.split('').reverse().join('');
  return withComma;
}

console.log(separateComma(333555))
console.log(separateComma(10005555));


// Your Own Tests (OPTIONAL)
function assert(test, message, test_number) {
  if (!test) {
    console.log(test_number + "false");
    throw "ERROR: " + message;
  }
  console.log(test_number + "true");
  return true;
}

assert(
  separateComma(1000) === "1,000",
  "The value should be a number.",
  "1. "
)

assert(
  separateComma(0) === "0",
  "The value should be a number.",
  "1. "
)

assert(
  separateComma(123) === "123",
  "The value should be a number.",
  "1. "
)

/* Reflection
What was it like to approach the problem from the perspective of JavaScript? Did you approach the problem differently?
We approached the problem the same as if we did with Ruby - we wrote out our
pseudocode and then tried to tackle the solution.

What did you learn about iterating over arrays in JavaScript?
Honestly, we didn't store our data within an array, we only used a string and then
looped over that string based on length and a counter.

What was different about solving this problem in JavaScript?
In my Ruby solution, I wasn't very dynamic in terms of my solution - I only
satisfied the problem according to the spec doc. So this time around, I definitely
focused more on being dynamic within the function. The main difference after that 
was just getting the correct syntax down.

What built-in methods did you find to incorporate in your refactored solution?
We used reverse methods and convert to a string method that allowed us to alter
the value of our number and input a comma after every third character.
*/