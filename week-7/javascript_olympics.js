 // JavaScript Olympics

// I paired with Jill Campbell on this challenge.

// This challenge took me [#] hours.


// Bulk Up
var athletes = [
  {name: "Sarah Huges", event: "Ladies' Singles"},
  {name: "Serena Williams", event: "US Open"},
  {name: "Michael Jordan", event: "Basketball"}
];

function addWin(array) {
  for (var x = 0; x < array.length; x++) {
    athletes[x]["win"] = function () {
    console.log(this.name + " is the world's best at " + this.event)
    }
  }     
}
addWin(athletes); 
athletes[1].win();


// // Jumble your words
function reverse_string(string){
  return string.split('').reverse().join('');
}

console.log(reverse_string("Hello how are you?"));


// 2,4,6,8
var numbers = [1,2,3,4,6,8,10];
function even_numbers(array) {
  var even_array = [];
  for (var num = 0; num < array.length; num++) {
    if (array[num] % 2 ==0) {
      even_array.push(array[num])
    }
  }
  return even_array
}

console.log(even_numbers(numbers))

// "We built this city"

function Athlete(name, age, sport, quote){
  this.name = name;
  this.age = age;
  this.sport = sport;
  this.quote = quote;
};

var michaelPhelps = new Athlete("Michael Phelps", 29, "swimming", "It's medicinal I swear!")

console.log(michaelPhelps.constructor === Athlete)
console.log(michaelPhelps.name + " " + michaelPhelps.sport + " " + michaelPhelps.quote)



/* Reflection
What JavaScript knowledge did you solidify in this challenge?
Syntax!!! It was really confusing compared to Ruby and I found myself starting off with Ruby syntax instead of
JS. However, I think I am finally good with JS syntax.

What are constructor functions?
A constructor in JS uses 'this', which is the object that owns the JS code. In our example, we created a method
that allowed us to use a constructor to identify name, sport, age and a quote.

How are constructors different from Ruby classes (in your research)?
Constructors are explicitly called using functions, where you use 'this' instead of '@'
for instance variables. Attributes of 'this' can be added and removed from a constructor 
whenever, whereas, instance variable you cannot.
*/