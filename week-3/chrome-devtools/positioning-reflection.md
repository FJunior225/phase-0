How can you use Chrome's DevTools inspector to help you format or position elements?
The DevTools inspector allows the developer to visually inspect each element of the webpage. This will allow the developer debug and track any layout issues in real time. If there are errors in the code, it will be represented in the DevTools inspector. 

How can you resize elements on the DOM using CSS?
In CSS, you can resize elements with the height and width value of a property. By adjusting the units in px or em, the developer can change the size of an element.

What are the differences between absolute, fixed, static, and relative positioning? Which did you find easiest to use? Which was most difficult?
Absolute positioning is positioned relative to the nearest ancestor - uses the body if element has no positioned ancestors. Fixed positioning is relative to the viewport and stays on the screen in that position when scrolling through the web browser. By using the top, right, bottom and left values, a developer can position the fixed element on any side of the screen. Static positioning is the default position of an element and are not affected by the top, right, bottom or left properties. Lastly, the relative property is when an element is positioned relative to its normal position. The fixed and absolute positioning seemed to be the easiest to use during this exercise - the most difficult was relative. I did not grasp the concept at first and needed visual representation to understand.

What are the differences between margin, border, and padding?
Margin is the space outside of the border - padding is the space between the content and the border - adjusting these elements either increases or decreases the space of these elements.

![alt text](imgs/D9nU6.gif)

What was your impression of this challenge overall? (love, hate, and why?)
I did not love or hate the challenege, I appreciated the concept and the visual representation it provided during each exercise. I was able to grasp the different concepts fully using the DevTools and adjust properties to see how they would reflect on the website. 

![alt text](imgs/change-color.png)
![alt text](imgs/column-stack.png)
![alt text](imgs/row.png)
![alt text](imgs/equidistant.png)
![alt text](imgs/squares.png)
![alt text](imgs/footer.png)
![alt text](imgs/header.png)
![alt text](imgs/sidebar.png)
![alt text](imgs/creative.png)