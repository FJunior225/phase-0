# Calculate the mode Pairing Challenge

# I worked on this challenge with: Kevin Serrano.

# I spent 1 hour on this challenge.

# Complete each step below according to the challenge directions and
# include it in this file. Also make sure everything that isn't code
# is commented.



# 0. Pseudocode

# What is the input? An array of either numbers (floats or integers) or strings
# What is the output? the most occurring element(s) from the input array in an array
# What are the steps needed to solve the problem?
# Create a new hash counter_hash
# FOR each element in the array
 # IF the element already exists in counter_hash
   # Increment that value by 1
 # ELSE
   # Add the element to the hash with a value of 1
# Initialize a variable frequency and set it to 0
# Initialize a variable word_array and set it to ['']
# FOR each key-value pair in counter_hash
 # Check if the value is greater than frequency
 # IF it is
   # Set frequency to value
   # Set word_array to the [key]
 # ELSE if value is equal to frequency
   # Add value to array word
# Return word


# 1. Initial Solution
# def mode(array)
#   counter_hash = Hash.new
#   array.each do |element|
#     if counter_hash.include?(element)
#       counter_hash[element] += 1
#     else 
#       counter_hash[element] = 1
#     end
#   end
#   frequency = 0
#   word_array = []
#   counter_hash.each do |key, value|
#     if value > frequency
#       frequency = value
#       word_array = [key]
#     elsif value == frequency
#       word_array += [key]
#     end
#   end
#   return word_array
# end



# 3. Refactored Solution
def mode(array)
  counter_hash = Hash.new
  array.each do |element|
    if counter_hash.include?(element)
      counter_hash[element] += 1
    else 
      counter_hash[element] = 1
    end
  end
  max = counter_hash.values.max
  new_hash = Hash[counter_hash.select {|key,value| value == max}]
  new_hash.keys
end


=begin 4. Reflection
Which data structure did you and your pair decide to implement and why?
We implemented a hash because it was easy to check to see if a key(number/string)
was present with its value(frequencies/number of times it repeated) and to return those
values. 

Were you more successful breaking this problem down into implementable pseudocode than the last with a pair? 
Yes, absolutely! My partner did an amazing job pseudocoding this challenge in a way that
made it extremely easy to transition into working code.

What issues/successes did you run into when translating your pseudocode to code?
We did not run into any issues - which I am still mind-blown about. I have never ran into
this situation and to have working code on the first attempt was the perfect example of
how good pseudocode can be extremely efficient.

What methods did you use to iterate through the content? Did you find any good ones when you were refactoring? Were they difficult to implement?
We used the include? to iterate over our hash to check to see if an element already
exists. Then we used a values and a max method stored within a variable and used that in
combination with a select method to check the value of frequency and add it to the array.

=end
