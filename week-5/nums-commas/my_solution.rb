# Numbers to Commas Solo Challenge

# I spent [] hours on this challenge.

# Complete each step below according to the challenge directions and
# include it in this file. Also make sure everything that isn't code
# is commented in the file.

# 0. Pseudocode

# What is the input? A positive integer
# What is the output? The positive integer as a string with commas.
# What are the steps needed to solve the problem?
# If the integer is <= 3 digits
	# return integer as string, no commas needed
# ELSE the integer is > 3 digits
	# Place a comma after every 3rd digit starting from first position
	# Return integer as a string 

# 1. Initial Solution
# def separate_comma(integer)
# 	if integer < 1000
# 		integer.to_s
# 	elsif integer >= 1000 && integer < 10000
# 		int_array = integer.to_s.split('')
# 		comma_array = int_array[0] + "," + int_array[1] + int_array[2] + int_array[3]
# 		return comma_array
# 	elsif integer >= 10000 && integer < 100000
# 		int_array = integer.to_s.split('')
# 		comma_array = int_array[0] + int_array[1] + "," + int_array[2] + int_array[3] + int_array[4]
# 		return comma_array
# 	elsif integer >= 100000 && integer < 1000000
# 		int_array = integer.to_s.split('')
# 		comma_array = int_array[0] + int_array[1] + int_array[2] + "," + int_array[3] + int_array[4] + int_array[5]
# 		return comma_array
# 	elsif integer >= 1000000 && integer < 10000000
# 		int_array = integer.to_s.split('')
# 		comma_array = int_array[0] + "," + int_array[1] + int_array[2] + int_array[3] + "," + int_array[-3] + int_array[-2] + int_array[-1]
# 		return comma_array
# 	elsif integer >= 10000000 && integer < 100000000
# 		int_array = integer.to_s.split('')
# 		comma_array = int_array[0]+ int_array[1] + ","  + int_array[2] + int_array[3] + int_array[4] + "," + int_array[-3] + int_array[-2] + int_array[-1]
# 		return comma_array
# 	end
# end

# 2. Refactored Solution
def separate_comma(integer)
	int_array = integer.to_s.split('')
	if integer < 1000
		integer.to_s
	elsif integer >= 1000 && integer < 10000
		comma_array = int_array[0] + "," + int_array[1] + int_array[2] + int_array[3]
		return comma_array
	elsif integer >= 10000 && integer < 100000
		comma_array = int_array[0] + int_array[1] + "," + int_array[2] + int_array[3] + int_array[4]
		return comma_array
	elsif integer >= 100000 && integer < 1000000
		comma_array = int_array[0] + int_array[1] + int_array[2] + "," + int_array[3] + int_array[4] + int_array[5]
		return comma_array
	elsif integer >= 1000000 && integer < 10000000
		comma_array = int_array[0] + "," + int_array[1] + int_array[2] + int_array[3] + "," + int_array[-3] + int_array[-2] + int_array[-1]
		return comma_array
	elsif integer >= 10000000 && integer < 100000000
		comma_array = int_array[0]+ int_array[1] + ","  + int_array[2] + int_array[3] + int_array[4] + "," + int_array[-3] + int_array[-2] + int_array[-1]
		return comma_array
	end
end



=begin 
3. Reflection
What was your process for breaking the problem down? What different approaches did you consider?
My process for breaking down the problem was remembering in a previous challenge about breaking words into an array using the split method.
So my first thought was turning the integer into a string, and then putting that broken up string into an array with split. From there
I can break up the integers by thousandths so I would only have to worry about the comma for each statement. Then I would rejoin the array
and add in the comma between the necessary array index. I originally thought of solving the solution as a whole, but thought I should probably
break down the problem as small as possible.

Was your pseudocode effective in helping you build a successful initial solution?
Yes, it was effective in the fact that it help me realize where to begin my initial solution - my pseudocode allowed me to realize where
commas had to go and then I could break down the problem into smaller pieces.

What new Ruby method(s) did you use when refactoring your solution? Describe your experience of using the Ruby documentation to implement it/them (any difficulties, etc.). Did it/they significantly change the way your code works? If so, how?
The only thing I refactored was the int_array on each statement and setting it equal outside of the conditional.

How did you initially iterate through the data structure?
I iterated over the original array using the split method, from there I was able to add each index back with commas as needed.

Do you feel your refactored solution is more readable than your initial solution? Why?
I feel that it definitely can be refactored even more - I am hesitant because I do not want to break the program and I feel I will
qjust go down a rabbit hole trying to find the right refactor.

=end
