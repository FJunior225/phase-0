# Method to create a list
# input: string of items separated by spaces (example: "carrots apples cereal pizza")
# steps: 
  # create hash with item & quantity elements
  # set default quantity
  # print the list to the console [use print_list method]
# output: hash data set

# Method to add an item to a list
# input: item name and optional quantity
# steps: 
	# push? (ruby docs) to add individual item to list
	# 
# output: updated hash with added item

# Method to remove an item from the list
# input: item name and quantity 
# steps:
	# pop?/delete? method to remove item
# output: updated hash with removed item

# Method to update the quantity of an item
# input: quantity of the item added or removed
# steps:
	# adjust value pair set 
# output: updated quantity hash

# Method to print a list and make it look pretty
# input: item with corresponding quantity
# steps:
	# p to display data to console & return value
# output: entire list with quantities

list = "carrots apples cereal pizza"

def create_list(item)
	string_array = item.split()
	string_hash = {}
	string_array.each do |item|
		string_hash[item] = 1	
	end
	p string_hash
end

def add_item(item, quantity, grocery_list)
	if grocery_list.include?(item)
		grocery_list += quantity
	else
		grocery_list[item] = quantity
	end
	p grocery_list	
end

def rem_item(item, grocery_list)
	grocery_list.delete(item)
end

def update_list(item, quantity, grocery_list)
	if grocery_list.include?(item)
		grocery_list[item] = quantity
	end
	p grocery_list
end

def print_list(grocery_list)
	grocery_list.each do |key, value|
		p "#{key}:" " #{value}"
	end
end

new_list = create_list(list)
add_item("Lemonade", 2, new_list)
add_item("Tomatoes", 3, new_list)
add_item("Onions", 1, new_list)
add_item("Ice Cream", 4, new_list)
rem_item("Lemonade", new_list)
update_list("Ice Cream", 1, new_list)
print_list(new_list)

=begin 
What did you learn about pseudocode from working on this challenge?
I learned that pseudocode is a very important aspect to grasp early on in my development
career. If my pseudocode is on point - I will have an easier time transitioning to
working code right off the bat and less time troubleshooting.

What are the tradeoffs of using Arrays and Hashes for this challenge?
The tradeoffs are that arrays can help organize your grocery list, however, if you 
also want to keep track of quantities, you will need a hash as well to be paired with
the item. You can begin with an array for converting the string of items into an array and
then add them to a hash with a default value of one. Once in the hash, you can then
use the methods to add more items to the hash with whichever quantity you would like.

What does a method return?
I would say that would depend on what is defined within the method - in this case, I am
returning the grocery list after each method occurs. Therefore, the output is either
a new item added to the list, an updated item or an item removed.

What kind of things can you pass into methods as arguments?
You can pass strings, integers, arrays, hashed etc. All of which we have used in this example.
Any object you can think of can be passed into methods as arguments.

How can you pass information between methods?
We can pass information between objects by using defined variables outside of the methods to
interact with - for example, I initiated a variable called new_list and set it equal to
the create_list method. I then reference that outside variable (meaning outside of the methods)
in all of the methods beyond that - add_item, rem_item, update_list & print_list.

What concepts were solidified in this challenge, and what concepts are still confusing?
The concept of allowing methods to interact with each other as well as how to create arrays
from strings and hashes from arrays were all solidified. Concepts that were still confusing
involved the bracket notation and some specific ruby concepts.


=end