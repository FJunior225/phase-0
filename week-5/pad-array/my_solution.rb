# Pad an Array

# I worked on this challenge with:Patrick Kelley.

# I spent [] hours on this challenge.


# Complete each step below according to the challenge directions and
# include it in this file. Also make sure everything that isn't code
# is commented in the file.



# 0. Pseudocode

# What is the input? 
# takes an initial array and a minimum size & optional padded value
# What is the output? (i.e. What should the code return?)
# if min size is less than array, return array
# if min size is greater than array, return array with "padding" up to minimum size
# What are the steps needed to solve the problem?
# compare array to minimum size
# if array size is >= minimum size, return original array
# if array size is < minimum size, return minimum size - length of array
# and add/join padding values to array.
# return updated array

# 1. Initial Solution
# def pad!(array, min_size, value = nil) #destructive
#   if min_size <= array.length or min_size == 0
#     array
#   elsif min_size > array.length
#     pad_size = min_size - array.length
#     padding_array = Array.new(pad_size, value)
#     ret_array1 = array.push(padding_array)
#     ret_array1.flatten!
#   end
# end

def pad(array, min_size, value = nil) #non-destructive
  if min_size <= array.length or min_size == 0
    array.flatten
  elsif min_size > array.length
    pad_size = min_size - array.length
    padding_array = Array.new(pad_size, value)
    return array + padding_array
  end
end

# 3. Refactored Solution
def pad!(array, min_size, value = nil) #destructive
  if min_size <= array.length 
    array
  elsif min_size > array.length
    pad_size = min_size - array.length
    padding_array = Array.new(pad_size, value)
    array.push(padding_array).flatten!
  end
end

=begin 4. Reflection
Were you successful in breaking the problem down into small steps?
Yes, to an extent. We were able to break down the problem, but happen to get stuck
in the #pad method. 

Once you had written your pseudocode, were you able to easily translate it into code? What difficulties and successes did you have?
We were able to get a first start where our code satisfied a few of the spec file's
code, and used the failures to adjust from there. However, we would of definitely 
been more suitable with better pseudocode - something we both need to practice.

Was your initial solution successful at passing the tests? If so, why do you think that is? If not, what were the errors you encountered and what did you do to resolve them?
Not even close! We had multiple errors and the toughest part was troubleshooting those errors.
We weren't sure about the built-in ruby methods and when we could use those, therefore, 
we could not resolve the problem without them and tried our hardest to do so. That took up the most time.

When you refactored, did you find any existing methods in Ruby to clean up your code?
We only found repeated code and I know for a fact that our refactored code can still be
refactored even more. Like I said, we suffered from poor pseudocode which lead to more time
spent on the initial solution.

How readable is your solution? Did you and your pair choose descriptive variable names?
I think we choose descriptive variables for another developer to read - as stated,
our code can definitely be more refactored, but I believe it is readable.

What is the difference between destructive and non-destructive methods in your own words?
Destructive methods are ones that end up altering an initial method, opposite of non-destructive. For example, the collect
method in ruby allows you to iterate over an array, but never changes the original array. This 
is an example of a non-destructive method. However, the collect! method not only allows you to
iterate over an array, but it also alters the original array to whatever you iterated over it with, changing
the original collection of information.

=end
