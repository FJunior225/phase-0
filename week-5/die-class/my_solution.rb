# Die Class 1: Numeric

# I worked on this challenge [by myself, with: ]

# I spent [] hours on this challenge.

# 0. Pseudocode

# Input: number of sides for a die
# Output: random roll of die
# Steps:
# initialize an instance variable for sides
	# raise and error if sides is < 1
# define sides
	# return number of sides for sides method
# define roll
	# return a random roll between 1 and number of sides 

# 1. Initial Solution

# class Die
#   def initialize(sides)
#   	if sides < 1 
#   		raise ArgumentError.new("Please pick a number greater than 1!")
#   	else @sides = sides
#   	end
#   end

#   def sides
#   	@sides
#   end

#   def roll
#   	rand(1..@sides)
#   end
# end

# 3. Refactored Solution

class Die
  def initialize(sides)
  	@sides = sides
  	raise ArgumentError.new("Please pick a number greater than 1!") if sides < 1
  end

  def sides
  	@sides
  end

  def roll
  	rand(1..@sides)
  end
end

=begin 
4. Reflection
What is an ArgumentError and why would you use one?
An AgumentError is when a method is called with the incorrect argument or parameter. For example, in our case we want an 
argument error to show when the sides called with the initialize is less than 1.

What new Ruby methods did you implement? What challenges and successes did you have in implementing them?
The #raise method was a new implementation that allows us to raise an error if a specific condition is met. Also, setting up 
instance variables was a new concept to implement. Understanding the logic was the hardest part for me and where to 
define to instance variables.

What is a Ruby class?
An object-oriented program language like that of ruby involves classes and objects. A class is basically a blueprint from 
which individual objects are created.

Why would you use a Ruby class?
Using classes allows you to group behaviors or methods into convenient bundles so that we can create many objects that
behave the same way. You can also add methods to individual objects but you don't have to do that with every object if 
you model your domain into classes.

What is the difference between a local variable and an instance variable?
A local variable is local to the place where it is defined (within a method), whereas an instance variable is visible 
anywhere else in the instance of the class in which it has been defined. 

Where can an instance variable be used?
It can be used within the class it is defined - if an initialize method has been defined with an instance variable, it can be
used within other methods of that class.

=end
