sea_lions_2016 = ["Alivia Blount","Alyssa Page","Alyssa Ransbury","Andria Reta","Austin Dorff","Autumn McFeeley","Ayaz Uddin","Ben Giamarino","Benjamin Heidebrink","Bethelhem Assefa","Bobby Reith","Dana Breen","Brett Ripley","Rene Castillo","Justin J Chang","Ché Sanders","Chris Henderson","Chris Pon","Colette Pitamba","Connor Reaumond","Cyrus Vattes","Dan Heintzelman","David Lange","Eduardo Bueno","Liz Roche","Emmanuel Kaunitz","FJ","Frankie Pangilinan","Ian Fricker","Ian Thorp","Ivy Vetor","Jack Baginski","Jack Hamilton","JillianC","John Craigie","John Holman","John Maguire","John Pults","Jones Melton","Tyler Keating","Kenton Lin","Kevin Serrano","wolv","Kyle Rombach","Laura Montoya","Luis Ybarra","Charlotte Manetta","Marti Osteyee-Hoffman","Megan Swanby","Mike London","Michael Wang","Michael Yao","Mike Gwozdek","Miqueas Hernandez","Mitchell Kroska","Norberto Caceres","Patrick Skelley","Peter Kang","Philip Chung","Phillip Barnett","Pietro Martini","Robbie Santos","Rokas Simkonis","Ronu Ghoshal","Ryan Nebuda","Ryan Smith","Saralis Rivera","Sam Assadi","Spencer Alexander","Stephanie Major","Taylor Daugherty","Thomas Farr","Maeve Tierney","Tori Huang","Alexander Williams","Victor Wong","Xin Zhang","Zach Barton"]

# input? method of an array
# output? accountability groups of people
# steps to take
# create list
# list.length % 4 == var
#   if var == 0
#     #slice into 4 equal groups.to_a
#     if var == 3
#       #slice into groups of 4 (slice_each) the last group will be a group of 3
#     if var == 2
# #       drop off six from list length and put them in two groups of three
#     if var == 1
#       drop off 5 from list length and put them in one group of five
  # slice up list evenly into groups of 4, assign each group a numer
  # if list.length % 5 == 0 
    #slice up list evenly into groups of 5


    
def acct_group_selection(array)
  rem = array.length % 4 # setting a variable to the remainder
  if rem == 0
    list_4_slice = array.each_slice(4).to_a # IF the array is evenly divided by 4 with no remainder - then group by 4
    acct_hash = Hash[list_4_slice.map.with_index { |value, index| [index, value] }] # Put array into hash with each key being a group of names
    # acct_hash.each_key { |key| key }
    #   key = "group" + [count] - attempt to rename keys 
  elsif rem == 3 # when remainder is 3 - divide each group by 4 and have the remaining 3 in their own group
    end_array = array.drop(array.length - 3)
    list_to_b_joined = end_array.each_slice(3).to_a
    list_4_slice = array.drop(3).each_slice(4).to_a
    #first join arrays 
    joined_array = list_4_slice + list_to_b_joined # combine array of all groups of 4 with one group of 3
    acct_hash = Hash[joined_array.map.with_index { |value, index| [index, value] }] # Put array into hash with each key being a group of names
  elsif rem == 2
    end_array = array.drop(array.length - 6) # when remainder is 2 - divide each group by 4 except one leaving a remainder greater than 2 
    list_to_b_joined = end_array.each_slice(3).to_a
    list_4_slice = array.drop(6).each_slice(4).to_a # remainder created is 6 - divide into 2 groups of 3
    #first join arrays 
    joined_array = list_4_slice + list_to_b_joined
    acct_hash = Hash[joined_array.map.with_index { |value, index| [index, value] }]
  elsif rem == 1 
    end_array = array.drop(array.length - 5) # when remainder is 1 - divide each group by 4 except one leaving a remainder greater than 2 
    list_to_b_joined = end_array.each_slice(5).to_a # leave remainder of 5 to their own group
    list_4_slice = array.drop(5).each_slice(4).to_a
    #first join arrays 
    joined_array = list_4_slice + list_to_b_joined # rejoin all arrays of 4 & 5
    acct_hash = Hash[joined_array.map.with_index { |value, index| [index, value] }]
  end
  p acct_hash
end

acct_group_selection(sea_lions_2016)

# def acct_group_selection(array)
# 	array.each_slice(5).to_a { |array| 
# 		p array}
# end

=begin 
What was the most interesting and most difficult part of this challenge?
THe most interesting was seeing how the logic would work - the difficult part was putting that logic into code. We manipulated
the remainder which is something we've never done before.

Do you feel you are improving in your ability to write pseudocode and break the problem down?
Yes, I definitely feel I am getting better in breaking the problem down to a point to get started - still need to work at it though.

Was your approach for automating this task a good solution? What could have made it even better?
It is a good start - the names are broken into groups by the way they are ordered in the array - we can add a random function that
would randomize the groups and get a different output each time the method is run. We can also make it better by adding a conditional
statement that runs if a student is dropped from an accountability group.

What data structure did you decide to store the accountability groups in and why?
We decided to store them into a hash where the group number is the key and its values are the students within that group. We figured that although the list
can be divided with the original array, displaying and organizing was easier with a hash.

What did you learn in the process of refactoring your initial solution? Did you learn any new Ruby methods?
Yes, I learned two new ruby functions - one is the drop method which will drop the end of an array by however many elements you
decide. The second is each_slice method which breaks up an array into sub-arrays of however many you decide. 
 
=end
