 ![site-map](../week-2/imgs/site-map.png "FJ's Site Map")

#Desgin-Reflection

What are the 6 Phases of Web Design?

The 6 phases include: 
*Info Gathering
*Planning
*Design
*Development
*Testing & Delivery
*Maintenance

What is your site's primary goal or purpose? What kind of content will your site feature?

My site will be about me. I will include my previous working and education experience; past projects; background; passions in life; as well as a blog. Therefore, it will be an informative website.

What is your target audience's interests and how do you see your site addressing them?

My target audience is my future employer and their interests' will be why I am a good candidate for their company. With any job search and sending of resumes, it needs to catch their attention on first look so I plan to make my website aesthetically appealing. 

What is the primary "action" the user should take when coming to your site? Do you want them to search for information, contact you, or see your portfolio? It's ok to have several actions at once, or different actions for different kinds of visitors.

First action would be to see my portfolio and information a bout me and then contact me.

What are the main things someone should know about design and user experience?

When implementing design, one must first consider the user experience associated with their website. After all, they are the customers of your website and you want them to keep coming back.

What is user experience design and why is it valuable? 

The user experience design is the feeling one gets when using a system. For example, those feelings can generate from usibility, efficiency, or perceptions of values associated with a system. 

Which parts of the challenge did you find tedious?

The part I found most tedious was getting the image path correct so that it would display correctly on GitHub.
