![Wireframe Index](/week-2/imgs/wireframe-index.png "Home Page")
![Wireframe Blog](/week-2/imgs/wireframe-blog-index.png "Blog Home Page")

What is a wireframe? : A wireframe is a mock-up design of your project (In our challenge it was a website). It shows how we want our design to format - not necessarily style.

What are the benefits of wireframing? : Wireframing gives the design team an idea of how the project should look and provides a smooth transition into the actual design phase. It provides a blueprint to show how the user will interact with your project (website, app ect.). 

Did you enjoy wireframing your site? : Yes I enjoyed wireframing my site. It gave me a perspective and the more I started using wireframes, the more I was able to see how I wanted my website to look.

Did you revise your wireframe or stick with your first idea? I definitely revised my original idea. At first, I was just going to use the template and change the words, but once I started moving things around, I was able to create an idea of how I wanted my site to look.

What questions did you ask during this challenge? One question was whether or not to sketch or use a wireframe template online. The second question was whether I should go further and create other pages besides my index and blog.

What resources did you find to help you answer them? I sought the guidance from a fellow Sea Lion and asked which he chose. 

Which parts of the challenge did you enjoy and which parts did you find tedious? The tedious part was once I had my idea, replicating that to show on the wireframe. Constantly having to add new elements was tedious. However, once I was finished with the wireframe, I was exicted to see how the finished project was going to look. That is the part I enjoyed the most.