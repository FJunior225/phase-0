1. SELECT * FROM states;
2. SELECT * FROM regions;
3. SELECT state_name, population FROM states;
4. SELECT state_name, population FROM states ORDER BY population DESC;
5. SELECT state_name FROM states WHERE region_id=7;
6. SELECT state_name, population_density FROM states WHERE population_density>50 ORDER BY population_density ASC;
7. SELECT state_name FROM states WHERE population BETWEEN 1000000 AND 1500000;
8. SELECT state_name, region_id FROM states ORDER BY region_id ASC;
9. SELECT region_name FROM regions WHERE region_name LIKE '%Central';
10. SELECT region_name, state_name FROM regions, states ORDER BY region_id ASC;

![alt text](./outfits-persons.png "Outline/Persons")


##What are databases for?
Databases are to store massive amounts of data to be referenced within a program. Yes, there are built-in objects to 
store data such as hashes and arrays, but we're talking about so much data that if we were to use those objects, the
program would take forever to load. Therefore, we use databases.

##What is a one-to-many relationship?
A one-to-many relationship is where one objects belongs to many of another object. In our challenge,
there were regions that were a collection of many states. Therefore, "states belong to regions and regions have
many states."

##What is a primary key? What is a foreign key? How can you determine which is which?
A primary key is something that is unique to a specific record, meaning there is only one. It is a unique
identifier and a relational database is required to have one primary key. A foreign key represents a record that
is unique to another table. In our challenge, regions had an ID field and states had a region_id field that was unique
to the regions table. Therefore, the region_id in the states table is considered a foreign key.

##How can you select information out of a SQL database? What are some general guidelines for that?
Using the SELECT keyword will allow you to select information out of a SQL database. There are several other keywords
involve that will allow you to manipulate how to see specific types of data such as WHERE, ORDER BY, LIKE, BETWEEN etc. 