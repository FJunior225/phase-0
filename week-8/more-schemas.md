![alt text](./imgs/persons_fingerPrint.png "Persons/Finger-Print")
![alt text](imgs/grocery_list_items.png "Grocery-List-Items")

###What is a one-to-one database?
In a one-to-one database, each row in table one is linked to one and only one other row in table two. Therefore, in my example, I used persons and fingerprints to show that each person has one unique set of fingerprints and fingerprints are only unique to one person.

###When would you use a one-to-one database? (Think generally, not in terms of the example you created).
Generally, I am not sure, because the two tables can be joined into one table. However, if there are rows that may not be accessed as often as the other, then having two tables with a one-to-one database would be best.

###What is a many-to-many database?
In a many-to-many database, one or more rows in a table can be related to 0, 1 or many rows in another table. In a many-to-many relationship between Table A and Table B, each row in Table A is linked to 0, 1 or many rows in Table B and vice versa. A 3rd table called a mapping table is required in order to implement such a relationship.

###When would you use a many-to-many database? (Think generally, not in terms of the example you created).
Generally, you would use this database when there are multiple relationships between two items/tables. Students and teachers, students and courses - with these two tables, each can act as categories or groupings for a set of entries for each other.

###What is confusing about database schemas? What makes sense?
I think the logic behind the concepts of relationships are easy to understand, but difficult to implement and create examples of definitions. I think the more practice we have creating schemas for different examples will help us.