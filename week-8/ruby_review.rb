# PezDispenser Class from User Stories

# I worked on this challenge [by myself, with: ].
# I spent [#] hours on this challenge.


# 1. Review the following user stories:
# - As a Pez user, I'd like to be able to "create" a new Pez dispenser with a group of flavors that represent Pez so it's easy to start with a full Pez dispenser.
# - As a Pez user, I'd like to be able to easily count the number of Pez remaining in a dispenser
#      so I can know how many are left.
# - As a Pez user, I'd like to be able to take a Pez from the dispenser so I can eat it.
# - As a Pez user, I'd like to be able to add a Pez to the dispenser so I can save a flavor for later.
# - As a Pez user, I'd like to be able to see all the flavors inside the dispenser so I know the order
#      of the flavors coming up.


# Pseudocode
# DEFINE dispenser
#  SET equal to hash of flavors with count for each flavor
# DEFINE method count_remaining
#  ITERATE through values in hash
#  SET counter at 0
#  ADD values to counter
#  RETURN counter
# DEFINE method remove_pez with two arguments (flavor and number - default 1)
#  USE flavor to access value in hash
#  SUBTRACT number (argument) from value
#  RETURN edited hash
# DEFINE method add_pez with two arguments (flavor and number - default 1)
#  USE flavor to access value in hash
#  ADD number (argument) to value
#  RETURN edited hash
# DEFINE method flavors
#  RETURN hash keys


# Initial Solution

class PezDispenser

  def initialize(i)
    @dispenser = {}
    i.each do |x| 
      @dispenser[x] = 1
    end
  end
  
  def pez_count
    counter = 0
    @dispenser.each_value do |x|
      counter += x
    end
    return counter
     
  end

  def get_pez # remove pez
    first = @dispenser.first[0]
    @dispenser.select! do |key, value|
      if value > 0
        @dispenser[key] = (value - 1)
        break
      end
    end  
    return first
    
  end
    
  def add_pez (flavor)
    if @dispenser.has_key?(flavor)
      @dispenser[flavor] += 1
    else 
      @dispenser[flavor] = 1
    end
    return @dispenser
  end
    
  def see_all_pez
    @dispenser.keys 
  end
  
  
end


# Refactored Solution
class PezDispenser

  def initialize(i)
    @dispenser = {}
    i.each {|x| @dispenser[x] = 1}  
  end
  
  def pez_count
    counter = 0
    @dispenser.each_value {|x| counter += x} 
    return counter   
  end

  def get_pez # remove pez
    first = @dispenser.first[0]
    @dispenser.select! {|key, value| value > 0 ? (@dispenser[key] = (value - 1)) : ()
        break 
      } 
    return first
  end
    
  def add_pez (flavor)
    @dispenser.has_key?(flavor) ? (@dispenser[flavor] += 1) :   @dispenser[flavor] = 1
    return @dispenser
  end
    
  def see_all_pez
    @dispenser.keys 
  end
  
end


# DRIVER TESTS GO BELOW THIS LINE

flavors = %w(cherry chocolate cola grape lemon orange peppermint raspberry strawberry).shuffle
super_mario = PezDispenser.new(flavors)
puts "A new pez dispenser has been created. You have #{super_mario.pez_count} pez!"
puts "Here's a look inside the dispenser:"
puts super_mario.see_all_pez
puts "Adding a banana pez."
super_mario.add_pez("banana")
puts "Now you have #{super_mario.pez_count} pez!"
puts "Oh, you want one do you?"
puts "The pez flavor you got is: #{super_mario.get_pez}"
puts "Now you have #{super_mario.pez_count} pez!"




# Reflection
# What concepts did you review or learn in this challenge?
# We reviewed the concepts of instance variables within a Class. We also reviewed methods such
# as has_key?, select! and each_value for hashes. 

# What is still confusing to you about Ruby?
# Iteration through a collection of data is still confusing to me - finding the right built-in
# methods to have the method do exactly I want it to do.

# What are you going to study to get more prepared for Phase 1?
# CODERWARS!! I definitely need to practice with more hands-on tutorials to increase my strengths with
# built-in methods.

