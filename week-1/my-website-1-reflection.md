# Paste a link to your [USERNAME].github.io repository.

http://fjunior225.github.io/

## Explain how to create a repository on GitHub and clone the repository to your local computer to a non-technical person.

I go to this website called GitHub, which is a cloud-based website that allows me to store my coding projects online. From that website, I create, what are called, repositories (folders) that I use to organize my projects. Once I create the repository online, I can then copy and paste it to my computer - therefore I'm bringing the folder I create online down to my computer locally. 

### Describe what open source means.

Open source is the ability to share source code for programs to the world so that others can use it and customize it to their needs. 

#### What do you think about Open Source? Does it make you nervous or protective? Does it feel like utopia?

Being a beginning in this industry, I happen to like open source. It gives a community a sense of unity and collaboration that you would not get otherwise. I can see how it might make an individual nervous or protective, but he or she is not perfect and their code may not be perfect. Therefore, keeping it open source allows for others to pick up on things the original owner may not have seen.

##### Assess the importance of using licenses.

Licenses provide a sense of rules when it comes to using the open source code for developers. If a license was not used for the website we created, people would be able to edit that website freely and put whatever they would like on it - sexual representation, racist remarks etc. and that would all be under your open source. Licenses prevent this work from being used in a way you would not accept.

###### What concepts were solidified in the challenge? Did you have any "aha" moments? What did you struggle with?

The concepts of cloning, making pull requests, merging pull request and pulling data down from GitHub were all solidified in this challenge. I felt more comfortable making a pull request and had an "aha" moment when I was able to pull the master branch back down after merging it with my second branch. I understood the process more efficiently and said "aha." I did not struggle in a specific area during this challenge.

####### Did you find any resources on your own that helped you better understand a topic? If so, please list it.

The benefits of using licenses and explaining them was an outside resource I used -- http://tinyurl.com/z4zu4o3
