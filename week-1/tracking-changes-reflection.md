# How does tracking and adding changes make developers' lives easier?

It allows developers to see how the project has grown and what each commit/change does to the project. Therefore, any bugs that may be introduce can be easily de-bugged by tracking previous changes.

## What is a commit?

A commit is a change or 'save point' to a given project.

### What are the best practices for commit messages?

First line is usually a header or similar to that of a subject line in an email, followed by a short summary (usually 50 words).

#### What does the HEAD^ argument mean?

HEAD^ is the last commit made and if you need to undo that commit, you would use HEAD^ to reset it.

##### What are the 3 stages of a git change and how do you move a file from one stage to the other?

*First, a change is made locally but not added yet.
*Second, that change is added (git add .) whcih makes the changes not staged for commit.
*Third, after git commit -m "", the changes to be committed will show and can now be pushed.

###### Write a handy cheatsheet of the commands you need to commit your changes?

*git status - checks the status of any changes
*git pull - updates GitHub with local copy 
*git add - changes are then able to be committed
*git commit -m "" - commits change with message
*git commit -v - opens text editor to add changes different way
*git push - pushes commits to GitHub

####### What is a pull request and how do you create and merge one?

A pull request brings down the GitHub copy of a project or branch to your local computer to work on and make changes to - creating one includes going to GitHub repo and creating a pull request directly from website. It will show changes you have made on your computer and pushed to GitHub. You can merge directly from website (alternatively, you can fetch and merge from your CLI).

######## Why are pull requests preferred when working with teams?

It keeps track of all changes made by every teammate.