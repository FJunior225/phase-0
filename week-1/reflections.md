# 1.1 Think About Time

After reviewing the resources provided to me on the “Think about time” challenge, I decided to focus on tiny habits. I watched a presentation by BJ Fogg and decided to do more research on his practice of tiny habits and how turning something so small can have a bigger impact on your life down the road. It was interesting to see how breaking down a goal into a small task that requires little motivation can turn into automation in your current lifestyle. The formula, “after I (insert existing routine), I will (insert new routine)” as explained by BJ Fogg in his presentation, uses your current actions you perform everyday as a trigger to the new routine. This method is definitely something I will try in my next behavioral change I want to accomplish. 

Time Boxing is a technique used to complete a goal in which a predetermined set of time is establish focused on the task (Agile Alliance & Institut Agile, 2013). Once that time is up, evaluation of what was accomplish takes place. This method can be useful during my time at DBC especially in Phase 0. Setting a time on challenges and assignments can help determine if my current strategy is efficient. This also allows for breaks which are helpful because I tend to stay focused on an assignment till completion and working straight through which can become frustrating at times and lead to taking longer to complete the assignment. 

 Currently I manage my time in my head. I state what needs to be done for the day in a sequential order and then perform my tasks. Things that I am highly motivated about and/or things of high priority often come first. I never allow for breaks whether it be for food or other and I never account for any mishaps. This strategy works for me because it allows my mind to work in order of operation and since my way of thinking is concrete sequential, I do not get stressed out of overwhelmed easily. However, I know this is not the most efficient way of working.

My overall plan for Phase 0 time management is to implement Time Boxing for assignments and challenges like this one. Since they are all marked short, medium, long y DBC, I will try to assign a Time Box based on those benchmarks - short (30-45 min) medium (1 hour - 1 hour and a half) long (2+ hours). This is a rough estimate based on this first assignment and will most likely change as the course goes on. Using this strategy, I can become more efficient in the time I have set aside and allow for better planning through each day of the week.

## 1.2 The Command Line

A shell is a command line interface that’s associated with an OS. Bash (Bourne
again shell) is a type of shell that is used as a programming language to
communicate to the computer via CLI.

The most challenging during this assignment was making sure I understand what
each command does and in what situations to use them. Making directories and
copying files are the easy ones, but using pushd and popd were more confusing. I
was able to use all of the commands successfully - the challenge now is to know
how to use them without looking them up.

In my opinion, the most important include pwd, mv, ls, cp, grep, mkdir because
these are the foundations for organizing your directories and files. Without knowing
these, it would be impossible to locate and store file efficiently. 

-pwd - print working directory
-ls - list directory
-mv - move directory
-cd - change directory
-../ - directory above the current directory
-touch - creates a new file
-mkdir - make new directory
-less - page through a file
-rmdir - removes directory
-rm - removes file
-help - offers help with commands

### 1.4 Forking and Cloning

When first starting this challenge, I was a little confused because when I learned about Git the first time, I was introduced to the git init command. After doing some research and finding clarity, I realized git init via CLI is similar to creating a repo on GitHub and I would recommend doing it on GitHub. Therefore, if I was going to write instructions, this is how it would look:

Creating a new Repo:
*venture to your account on www.GitHub.com
*click “+” symbol on top right of page
*click “New Repository”
*Create a name for repo
*select whether you would like it to be public or private
*click create repo

Fork a repo:
*On the page of the repo you want to fork, click “fork” button on the top right-hand side
*select your individual profile to fork to

Clone a repo:
*After forking to individual profile - copy the https url
*go to your CLI
*go to directory you want to clone the repo
*enter “git clone (paste copied url)” and hit enter

Forking a repo instead of creating a new one will allow an individual to work off of someone else’s project. For example, as students, we have challenges given to us from DBC and in order to work on those challenges by ourselves, we have to fork it from DBC and then submit our changes without affecting the original copy from DBC.

What struggles did you have setting up git and GitHub? What did you learn in the process?
The struggle I had setting up git and GitHub was setting up my environment. I had previously set up my git to a specific setting by a different organization so changing it back, I was not sure if I did it correctly and it took longer than as if I was doing it for the first time. In the process, I learned more about bash profiles and how well you can customize your terminal.