#Attr Methods

# I worked on this challenge by myself.

# I spent [#] hours on this challenge.

# Pseudocode

# Input: Name 
# Output: greeting which includes that name
# Steps:
# Initialize name variable within the NameData class
# Initialize an instance of NameData
# Output a string

class NameData
	attr_reader :name

	def initialize
		@name = "FJ"
	end
end


class Greetings
	def initialize
		@greet_person = NameData.new
	end

	def hello
		puts "Hello #{@greet_person.name}! How wonderful to see you today."
	end
end

greet = Greetings.new
greet.hello

=begin
Reflection
Release 1
What are these methods doing?
These methods are storing information about an individual and then changing that
information with different methods.

How are they modifying or returning the value of instance variables?
With the change_my_age and change_my_name methods, the program can be altered
to have those variables change when being called.

Release 2
What changed between the last release and this release?
The attr_reader was included in the beginning of the code and the method 
what_is_age, was deleted.

What was replaced?
The attr_reader replaced the what_is_age method - since the what_is_age method
was only returning the value of @age, the attr_reader was able to do the same thing
in less code, hence the replacement.

Is this code simpler than the last?
Yes, this code is simpler than the last and easy to understand.

Release 3
What changed between the last release and this release?
The attr_writer was now added to the code for @age. Also the method for change_my_age
has now been removed.

What was replaced?
The method for changing the age has been replaced. Since the attr_writer is able 
to change the value of a  variable outside of the class, the method was no longer
needed.

Is this code simpler than the last?
Yes, it is simpler than the last and easy to understand.

Release 5
What is a reader method?
A reader method returns the value of a variable outside of the class, but does not change it.

What is a writer method?
A writer method allows you to change the value of a variable outside of the class, but 
does not return it.

What do the attr methods do for you?
It allows you to manipulate code or return code without having to define a separate
method for it. 

Should you always use an accessor to cover your bases? Why or why not?
No, sometimes all you want the code to do is return its value of a variable. Having
each variable changeable can lead to security risks as well as having a difficult time
debugging your code. Therefore, you should only use what is meant to be used.

What is confusing to you about these methods?
Keeping track of all your variables and which accessor method you gave them. It can 
seem like a lot is going on, but you just have to keep practicing with these attr methods
so that you know what you're doing.
=end