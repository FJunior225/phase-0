# A Nested Array to Model a Bingo Board SOLO CHALLENGE

# I spent [#] hours on this challenge.

# Create a method to check whether that column has that number in the existing bingo_board.
# If the number is in the column, replace it with an "X".
# Display the new board to the console (Make it pretty).

# Release 0: Pseudocode
# Outline:

# Create a method to generate a letter ( b, i, n, g, o) and a number (1-100)
  # create an array with letters, B, I, N, G, O.
  # choose a random index
  # select a random number between 1-100 and join it with the letters array
  # return the value as a string

# Check the called column for the number called.
  # create a variable for number called
  # How do I get each letter to represent each array index?
  # create a hash with numbers as values and letters as keys?
  # number called 


# If the number is in the column, replace with an 'x'
  # If the number called is equal to number in the column
    # replace number with an 'X' (using index as reference)

# Display a column to the console
  # return the array with 'X' in it

# Display the board to the console (prettily)
  # return the entire board with 'X' marked

# Initial Solution

# class BingoBoard

#   # attr_reader :bingo_board

#   def initialize(board)
#     @bingo_board = board
#     # @letter_index = (0..@bingo_ary.length - 1)
#   end

#   def generator
#     @bingo_ary = ['B', 'I', 'N', 'G', 'O']
#     @letter = @bingo_ary.sample
#     @number = rand(1...100)
#    puts "The Bingo number is #{@letter + @number.to_s}!"
#   end

#   def check_column
#     # puts "the letter index is #{@letter_index} which represents #{@bingo_ary[@letter_index]}"
#     @bingo_board.each do |sub_array|
#       if sub_array[@letter.to_i] == @number
#           sub_array[@letter.to_i]== "X"
#       end
#     end
#     display_board
#   end

#   def display_board
#     p "[B - I - N - G - O]"
#     p "-------------------"
#     @bingo_board.each { |sub_array| 
#       p sub_array.to_s}
#   end
# end



# Refactored Solution

class BingoBoard

  attr_reader :bingo_board

  def initialize(board)
    @bingo_board = board
  end

  def generator
    @bingo_ary = %w(B I N G O)
    @letter = @bingo_ary.sample
    @number = rand(1...100)
   puts "The Bingo number is #{@letter + @number.to_s}!"
  end

  def check_column
    # puts "the letter index is #{@letter_index} which represents #{@bingo_ary[@letter_index]}"
    @bingo_board.each do |sub_array|
      if sub_array[@letter.to_i] == @number
          sub_array[@letter.to_i]== "X"
      end
    end
    display_board
  end

  def display_board
    p "[B - I - N - G - O]"
    p "-------------------"
    @bingo_board.each { |sub_array| 
      p sub_array.to_s}
  end
end

# #DRIVER CODE (I.E. METHOD CALLS) GO BELOW THIS LINE
board = [[47, 44, 71, 8, 88],
        [22, 69, 75, 65, 73],
        [83, 85, 97, 89, 57],
        [25, 31, 96, 68, 51],
        [75, 70, 54, 80, 83]]

new_game = BingoBoard.new(board)
5.times do
  p new_game.generator
  new_game.check_column
end

=begin
Reflection
How difficult was pseudocoding this challenge? What do you think of your pseudocoding style?
The way the challenge was set up, it was not difficult to pseudocode for this challenge. The logic
was clear and concise and I was able to write out each step. However translating my
pseudocode into actual code was my real challenge.

What are the benefits of using a class for this challenge?
The methods within the class can interact with each other so that each step of the bingo
game can help update the user's current board. Without using a class, it would be
extremely difficult to create this game.

How can you access coordinates in a nested array?
There are a couple different ways to access coordinates within a nested array - one is
using bracket notation to select a specific coordinate(i.e. @bingo_board[0][1]). Another 
way is iterating over each nested array 
(i.e. @bingo_board.each do |sub_array|
      if sub_array[@letter.to_i] == @number
          sub_array[@letter.to_i]== "X"
      end )
Both methods can be used, however I feel the iteration is my dynamic.

What methods did you use to access and modify the array?
I used #each with the combination of bracket notation to access and modify the array.

Give an example of a new method you learned while reviewing the Ruby docs. Based on what you see in the docs, what purpose does it serve, and how is it called?
After spending countless hours on this challenge and still not fully sure if I correctly understand
my solution or if it works, I did learn a few new methods that I did not implement. For me,
the understanding of iterating over nested arrays with #each was a new experience and one I need
to keep practicing on.

How did you determine what should be an instance variable versus a local variable?
If I needed to refer back to a specific variable in different methods, I choose to make it
into an instance variable. If that variable was only used within a specific method once, I choose
to leave it as a local variable.

What do you feel is most improved in your refactored solution? 
The thing I improved on was finding more efficient way to write the same code. For
example, creating a new array can be as simple as @bingo_ary = %w(B I N G O). Using
neat little tricks like that can really improve on readability and efficiency within my
code.
  
=end
