# Your Names
# 1) Mike London
# 2) FJ Collins

# We spent [1] hours on this challenge.

# Bakery Serving Size portion calculator.

def serving_size_calc(food_to_make, num_of_ingredients) # defines a method that takes in two arguements
  pantry = {"cookie" => 1, "cake" =>  5, "pie" => 7} #hash of things to make with its ingredients

 
raise ArgumentError.new("#{food_to_make} is not a valid input") if pantry.has_key?(food_to_make) == false

  serving_size = pantry[food_to_make]
  
  remaining_ingredients = num_of_ingredients % serving_size #storing value of number of ingredients % serving_size
  
  
  # remaining_ingredients == 0 ? return true : return false 
  meal_count = num_of_ingredients / serving_size

  return "Made #{meal_count} #{food_to_make}" if remaining_ingredients == 0
  
  return "Made #{meal_count} #{food_to_make}, you have #{remaining_ingredients} leftover ingredient(s)."
  
  
  # if remaining_ingredients == 0
  #   return "Made #{meal_count} #{food_to_make}"
  # elsif remaining_ingredients == 1
  #   return "Made #{meal_count} #{food_to_make}, you have #{remaining_ingredients} leftover ingredient."
  # else
  #   return "Made #{meal_count} #{food_to_make}, you have #{remaining_ingredients} leftover ingredients."
  # end
end

p serving_size_calc("pie", 7)
p serving_size_calc("pie", 8)
p serving_size_calc("cake", 5)
p serving_size_calc("cake", 7)
p serving_size_calc("cookie", 1)
p serving_size_calc("cookie", 10)
p serving_size_calc("cupcake", 5) # expect ArgumentError to be raised 

=begin 
Reflection
What did you learn about making code readable by working on this challenge?
It definitely helps to have readable code - at first glance, it was hard to
understand exactly what the program was trying to do. There were a couple of
redundant code and confusing iterations done that were not necessary. Readable
code makes it easier to modify and add features.

Did you learn any new methods? What did you learn about them?
I learned the #values_at method for hashes, even though we refactored to not
use it (due to redundancy). I also learned about ternary operator and refreshed
my memory about how to use it and when to use it.

What did you learn about accessing data in hashes? 
It's a lot easier using bracket notation to reference keys and values from
a hash - also using methods such as #has_keys? is easy to see if a key is 
currently present.

What concepts were solidified when working through this challenge?
There weren't any concepts that needed to be solidified during this challenge,
however, it was good for refactoring purposes and getting a sense of DRY code.

=end