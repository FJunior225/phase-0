# Die Class 2: Arbitrary Symbols


# I worked on this challenge by myself.
# I spent [.5] hours on this challenge.

# Pseudocode

# Input: array of strings
# Output: random roll of strings within the array
# Steps:
# initialize an instance variable for labels
	# raise an error if empty array is passed
# initialize an instance variable for sides to equal number of items in array
# define sides
	# return number of sides for sides method
# define roll
	# return a random roll between array of strings

# Initial Solution

# class Die
#   def initialize(labels)
#   	@labels = labels
#   	@sides = labels.length
#   	raise ArgumentError.new("Sorry, but an empty array was used!") if labels.length == 0
#   end

#   def sides
#   	@sides
#   end

#   def roll
#   	@labels[rand(@labels.length)]
#   end
# end



# Refactored Solution
class Die
  def initialize(labels)
  	@labels = labels
  	@sides = labels.length
  	raise ArgumentError.new("Sorry, but an empty array was used!") if labels.empty?
  end

  def sides
  	@sides
  end

  def roll
  	@labels.sample
  end
end


=begin
Reflection
What were the main differences between this die class and the last one you created in terms of implementation? Did you need to change much logic to get this to work?
The difference was another variable needed to be initialized for the array of strings (@labels). The same logic was used
as in the last die class challenge - just needed to point sides to be equal to the length of the array.

What does this exercise teach you about making code that is easily changeable or modifiable?
It is super efficient to make code that is easily changeable or modifiable. I did not have to rethink much of the logic - I just
had to point variables differently.

What new methods did you learn when working on this challenge, if any?
I learned more about the #sample method to randomize within an array. I was able to refactor my solution to instead of focusing 
on randomizing the length of the array, I can just use sample to randomly pick within the array.

What concepts about classes were you able to solidify in this challenge?
I still need more work with Classes as a whole, but I was able to get an understanding of how each method operates with each
other within the Class.

=end