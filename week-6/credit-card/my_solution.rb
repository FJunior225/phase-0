# Class Warfare, Validate a Credit Card Number


# I worked on this challenge with Pietro.
# I spent [.85] hours on this challenge.

# Pseudocode

# Input: 16 digit number
# Output: boolean for valid credit card number
# Steps:
# Initialize number variable 
# Check size of number 
	# if not 16, raise an ArgumentError
# define a method of conversion
# 	convert number to a string 
# 	separate each digit into an array
# 	convert back to an integer
# 	double every other number
# define a method for sum
# 	convert numbers back into strings
# 	separate the strings individually
# 	flatten the array and convert strings back into integers
# 	sum of all integers
# define a method for validation
# 	check if sum is divisible by 10
# 	if yes, return true
# 	else return false

# Initial Solution

# Don't forget to check on initialization for a card length
# of exactly 16 digits

# class CreditCard
  
#   def initialize(number)
#     if number.to_s.length == 16
#       @number = number
#     else
#       raise ArgumentError.new ("Please enter a 16 digit number!") 
#     end 
#   end
  
#   def convert
#     @convert = @number.to_s.chars.reverse.map(&:to_i).map.with_index { |num, i| i % 2 == 0 ? num : num * 2 }
#   end 
  
#   def sum
#     @sum = @convert.map(&:to_s).map(&:chars).flatten.map(&:to_i).reduce(:+)
#   end
  
#   def check_card
#   	convert
#   	sum
#     if @sum % 10 == 0
#       return true
#     else
#       return false
#     end  
#   end

# end


# credit = CreditCard.new(1234567812345678)
# p credit.convert
# p credit.sum
# p credit.check_card

# Refactored Solution
class CreditCard
  
  def initialize(number)
    if number.to_s.length == 16
      @number = number
    else
      raise ArgumentError.new ("Please eneter a 16 digit number!") 
    end 
  end
  
  def convert
    @convert = @number.to_s.chars.reverse.map(&:to_i).map.with_index { |num, i| i % 2 == 0 ? num : num * 2 }
  end 
  
  def sum
    @sum = @convert.map(&:to_s).map(&:chars).flatten.map(&:to_i).reduce(:+)
  end
  
  def check_card
    convert
    sum
    @sum % 10 == 0 ? true : false     
  end

end

=begin
Reflection
What was the most difficult part of this challenge for you and your pair?
For us, it was the check_card method - at first, we just used the instance 
variable @sum and used it within a boolean, but it did not pass the rspec.
We had to call the previous methods in order for #check_card to work.

What new methods did you find to help you when you refactored?
I learned a lot of new methods/semantics when refactoring. The easiest was the
ternary operator for the #check_card. Since the IF & ELSE was simple enough,
we figured ternary would be readable and efficient. However, for #initialize, 
we agreed not to use ternary operator because of readability. Also, map.with_index
was a key part to use the even indexes within the array and double those numbers
specifically. 

What concepts or learnings were you able to solidify in this challenge?
I'm still a little confused on scope within a Class and need some clarification on
why #convert & #sum needed to be called in order for #check_card to operate
correctly with the rspec. However, I am slowly solidifying Enumerables and 
the methods used with them.

=end
