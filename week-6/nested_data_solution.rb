#nested_array = [1, ["inner", "array"], 2, 3]

# RELEASE 2: NESTED STRUCTURE GOLF
# Hole 1
# Target element: "FORE"

array = [[1,2], ["inner", ["eagle", "par", ["FORE", "hook"]]]]

# attempts:
# ============================================================
p array[1][1][2][0]

# ============================================================

# Hole 2
# Target element: "congrats!"

#hash = {outer: {inner: {"almost" => {3 => "congrats!"}}}}

# attempts:
# ============================================================
p hash[:outer][:inner]["almost"][3]


# ============================================================


# Hole 3
# Target element: "finished"

nested_data = {array: ["array", {hash: "finished"}]}

# attempts:
# ============================================================
p nested_data[:array][1][:hash]


# ============================================================

# RELEASE 3: ITERATE OVER NESTED STRUCTURES

nested_array.each do |element|
  if element.kind_of?(Array)
   element.each {|inner| p inner}
  end
end


number_array = [5, [10, 15], [20,25,30], 35]

number_array.each do |element|
  if element.kind_of?(Integer)
   p element + 5
  end
  
  if element.kind_of?(Array)
    element.each {|inner| p inner + 5}
  end
end


# Bonus:

startup_names = ["bit", ["find", "fast", ["optimize", "scope"]]]

# startup_names[0]
# startup_names[1][0]
# p startup_names[1][1
# p startup_names[1][2][0]
# p startup_names[1][2][1]

 # Obviously, not the intended method to use for nested structures, but it works. 
new_array = startup_names.flatten 
  new_array.each do |word|
    p word.concat("ly")
end


# if element.kind_of?(String)
# 	p element.concat("ly")
# end
  
# if element.kind_of?(Array)
#   p element.each { |word| p word.to_s.concat("ly")} 
# end
  
# if element.kind_of?(Array)
#   p element.each {|most_inner| p most_inner.to_s.concat("ly")}
# end

=begin

What are some general rules you can apply to nested arrays?
One general rule you can apply is that nested arrays are all different. A good way
to remove the nesting is using #flatten, however if not all of the elements within
the nested structure are not the same, it is difficult to manipulate the code. 

What are some ways you can iterate over nested arrays?
One way, you can iterate over nested arrays is to iterate over each array within the 
nested structure with #each. Another way is using the #kind_of? which allowed
us to see which data structure was inside the array, and then perform some
action on that data structure based on the response.

Did you find any good new methods to implement or did you re-use one you were already familiar with? What was it and why did you decide that was a good option?
I learned about the #kind_of? method which is interesting, however not entirely dynamic.
The struggle is figuring out how to access each data structure within the nested
array. We thought this was a good option to manipulate the data within, and it was a
new method to learn.

=end
