# Build a simple guessing game


# I worked on this challenge [by myself, with: ].
# I spent [#] hours on this challenge.

# Pseudocode

# Input: an integer 
# Output: return true if guess is correct, high if guess is higher than answer, low if lower than answer
# Also should output a solved method, true or false if last guess is correct answer
# Steps:
# Initialize guess 
# initialize answer
# Define guess which takes in an integer
# 	IF the guess is < the answer 
# 		return :high
# 	IF the guess is > the answer
# 		return :low
# 	ELSE the guess == the answer
# 		return :correct
# Define solved? 
	# which returns true if most recent guess is correct
	# ELSE false



# Initial Solution

# class GuessingGame
#   def initialize(answer)
#   	@answer = answer
#   	@solved = false
#   end

#   def guess(guess)
#   	@guess = guess
#   	@solved = false
#   	if guess < @answer
#   		return :low
#   	elsif guess > @answer
#   		return :high
#   	else 
#   		@solved = true
#   		return :correct
#   	end
#   end

#   def solved?
#   	@solved
#   end
# end

# game = GuessingGame.new(22)
# game.guess(12)
# game.solved?


# Refactored Solution
class GuessingGame
  def initialize(answer)
  	@answer = answer
  	@solved = false
  end

  def guess(guess)
  	@guess = guess
  	@solved = false
  	return :low if guess < @answer
  	return :high if guess > @answer
  	@solved = true
  	return :correct
  end

  def solved?
  	@solved
  end
end

=begin
Reflection
How do instance variables and methods represent the characteristics and behaviors (actions) of a real-world object?
We can use ourselves, humans as examples. A human class can have many instance variables 
such as eye color, hair color, skin color, right-handed, left-handed, ambidextrous
etc. Humans can also have methods such as thinking, speaking, moving just to specify
the basics. It can get very complex, but that is a real-life example.

When should you use instance variables? What do they do for you?
Instance variables are useful when you have a variable that is used over and over again.
Within the class, instead of defining a local variable more than once, you can
define an instance variable and call it throughout the different methods.

Explain how to use flow control. Did you have any trouble using it in this challenge? If so, what did you struggle with?
Flow control is the control of logic through different conditionals such as if/else, when statements,
while/until etc. The part I had some difficulty with was with the @solved instance variable
and where to input it within my if statements. 

Why do you think this code requires you to return symbols? What are the benefits of using symbols?
Well, symbols cannot be altered or changed when established, and since this game had a definitive
process where your guess has only three options (high, low or correct), it make sense to use
symbols. They are unique like integers but can also act like strings. 

=end