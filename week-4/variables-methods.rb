puts "Hello, what is your first name?"
first_name = gets.chomp
puts "Hello #{first_name}, what is your middle name?"
middle_name = gets.chomp
puts "Well hello, #{first_name} #{middle_name}! Lastly, please tell me your last name?"
last_name = gets.chomp
puts "It is a pleasure to meet you #{first_name} #{middle_name} #{last_name}!"

puts "What is your favorite number?"
favorite_number = gets.chomp
bigger = favorite_number.to_i + 1
puts "I believe #{bigger} is a bigger and better favorite number!"


=begin 
https://github.com/FJunior225/phase-0/blob/master/week-4/define-method/my_solution.rb
https://github.com/FJunior225/phase-0/blob/master/week-4/address/my_solution.rb
https://github.com/FJunior225/phase-0/blob/master/week-4/math/my_solution.rb

How do you define a local variable?
Local variables are variables that are set locally within a program beginning
with a lowercase letter. They do not hold any arguements and can be set to a 
string or integer as long as its not set to another variable - things become 
more difficult. 

How do you define a method?
Methods are used liked verbs in the Englsh language. They perform some type of action
within a program - usually for repetitive code. A method can have none or 
multiple parameters that allow the method to call several objects. For example,

def method
	puts "Hello World"
end

method
method

This will allow the program to say "Hello World" as many times as needed without
having to repeat the puts method.

What is the difference between a local variable and a method?
A local variable can store information much like a method does, however, a method
can call to multiple objects whereas a local variable can only call to one (whatever
is stored inside of it). Methods can also be used in very complex situations whereas
local variables cannot. 

How do you run a ruby program from the command line?
By using the command ruby filename.rb

How do you run an RSpec file from the command line?
By using the command rspec filename_spec.rb

What was confusing about this material? What made sense?
I did not find much confusing as it is all refreshing information from the interview
process for DBC. I found everything to make sense thus far - I do get confused with
spec files and look forward to learning more about them.

=end