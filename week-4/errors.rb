# Analyze the Errors

# I worked on this challenge by myself.
# I spent [.75] hours on this challenge.

# --- error -------------------------------------------------------

# "Screw you guys " + "I'm going home." = cartmans_phrase

# This error was analyzed in the README file.
# --- error -------------------------------------------------------

def cartman_hates(thing)
  	while true
    	puts "What's there to hate about #{thing}?"
	end
end

# This is a tricky error. The line number may throw you off.

# 1. What is the name of the file with the error?
# => The name of the file is errors.rb
# 2. What is the line number where the error occurs?
# => The error shows the line number to be 170, which is the end of the document.

# 3. What is the type of error message?
# => This is a syntax error, meaning the code itself is incorrect. It looks to show that 
# a method was defined to take in a single parameter, however the while loop
# does not have an end to it.
# 4. What additional information does the interpreter provide about this type of error?
# => The error message shows that there is an unexpecting an end-of-input
# and that it's expeting an end to the code.
# 5. Where is the error in the code?
# => The error lies with the while loop not having an end to it's loop.
# 6. Why did the interpreter give you this error?
# => The reason the interpreter gave the error to be on line 170 is because of
# a domino effect - although the while loop does not have an end on it, the next
# line of code does have an end (which looks to be intended for the method)
# however, the program reads code line by line and according to the program,
# the while loop does have an end, therefore, the program is expecting a end of input
# somewhere at the end of all of the code (which is line 170).

# --- error -------------------------------------------------------

south_park = "Comedy cartoon"

# 1. What is the line number where the error occurs?
# => This error takes place on line 45.
# 2. What is the type of error message?
# => This type of error is a name error. This occurs when a name is undefined
# or invalid.
# 3. What additional information does the interpreter provide about this type of error?
# => The interpreter says there is an undefined local variable 'south_park'.
# 4. Where is the error in the code?
# => The local variable 'south_park' is not defined by the user - therefore
# there needs to be an '=' with some sort of value afterwards.
# 5. Why did the interpreter give you this error?
# => As stated above, the local variable is undefined and the program does
# not know what 'south_park' is without an assigned value.

# --- error -------------------------------------------------------

def cartman()
	puts "Screw you guys, I'm going home!"
end
cartman()

# 1. What is the line number where the error occurs?
# => This error takes place on line 63.
# 2. What is the type of error message?
# => This is a no method error - this occrus because a method that is undefined or
# invalid is being called.
# 3. What additional information does the interpreter provide about this type of error?
# => The interpreter says there is an undefined method 'cartman'.
# 4. Where is the error in the code?
# => Since the method does not have an object within it, the program does
# not understand what to do when the method is called. The method must first
# be defined so that when it is called, the program knows what output is used.
# 5. Why did the interpreter give you this error?
# => The user did not define the method first before calling it.

# --- error -------------------------------------------------------

def cartmans_phrase(phrase)
  phrase
end

cartmans_phrase('I hate Kyle')

# 1. What is the line number where the error occurs?
# => This error takes place on line 84.
# 2. What is the type of error message?
# => This error is an arguement error, which occurs when the arguements for 
# a method are incorrectly called or invalid.
# 3. What additional information does the interpreter provide about this type of error?
# => The interpreter states there is a wrong number of arguements (1 for 0). It
# also states the error comes from line 88 (main).
# 4. Where is the error in the code?
# => The error is with the calling of the method - the method was defined without
# any parameters and when it is called, it has a parameter "I hate Kyle".
# 5. Why did the interpreter give you this error?
# => The error was given because the user defined a method with no parameters
# and then called the method with a parameter - the program does not know
# how to use the parameter when the method is not defined with one.

# --- error -------------------------------------------------------

def cartman_says(offensive_message)
  puts offensive_message
end

cartman_says("You're mom is so fat!")

# 1. What is the line number where the error occurs?
# => This error takes place on line 108.
# 2. What is the type of error message?
# => It is another arguement error - however, this one is an opposite of the last.
# 3. What additional information does the interpreter provide about this type of error?
# => The interpreter states there are wrong number of arguements (0 for 1) and
# that is comes from errors having to do with line 112.
# 4. Where is the error in the code?
# => THe issue is a method is defined with a parameter, but when it is called
# there is no parameter with it. Methods cannot be defined and called differently.
# 5. Why did the interpreter give you this error?
# => The program cannot comprehend what the user is intending here - it knows
# a method is defined with one parameter, but is called without it - it has to
# be exactly the same in order for the program to understand and compute.



# --- error -------------------------------------------------------

def cartmans_lie(lie, name)
  puts "#{lie}, #{name}!"
end

cartmans_lie('A meteor the size of the earth is about to hit Wyoming!', 'Kyle')

# 1. What is the line number where the error occurs?
# => This error takes place on line 133.
# 2. What is the type of error message?
# => This is another arguement error, where a method was defined with two
# parameters, but was only called with one.
# 3. What additional information does the interpreter provide about this type of error?
# => The interpreter states there are wrong number of arguements (1 for 0). As
# like the one above, 1 for 0 means there isn't a parameter being called with
# the method. The interpreter also states this error comes from line 137.
# 4. Where is the error in the code?
# => The error is that the defined method is only being called with one 
# parameter when it was defined with two.
# 5. Why did the interpreter give you this error?
# => Again, the interpreter understands this method to be called with two
# parameters and cannot computer when only one is called.

# --- error -------------------------------------------------------

"Respect my authoritay!" * 5

# 1. What is the line number where the error occurs?
# => This error occurs on line 157.
# 2. What is the type of error message?
# => This is a type error - occurs when an object is not of the expecting type.
# 3. What additional information does the interpreter provide about this type of error?
# => The interpreter shows the multiplication sign and states a string cannot be
# coerced into a fixed number.
# 4. Where is the error in the code?
# => The error is that the user is trying to multiply a fixed number by a string.
# 5. Why did the interpreter give you this error?
# => The program does not understand what the user is doing - The program
# reads this as 'respect my authoritay!' sets of 5 which does not make sense.
# If the user switched it around - thr porgram can compute that to mean 5
# sets of 'Respect my authoritay!'

# --- error -------------------------------------------------------

amount_of_kfc_left = 20/5


# 1. What is the line number where the error occurs?
# => This error occurs on line 176.
# 2. What is the type of error message?
# => This is a zero division error - occurs when an integer is being divided by 0.
# 3. What additional information does the interpreter provide about this type of error?
# => Shows the division symbol and states divided by zero which is an error.
# 4. Where is the error in the code?
# => The error is which the number 20 being divided by 0.
# 5. Why did the interpreter give you this error?
# => The program recognizes numbers based on the IEEE 754 Floating Point Standard.
# This standard does not recognize a division by zero to be a number, hence the error.

# --- error -------------------------------------------------------

# require_relative "cartmans_essay.md"

# 1. What is the line number where the error occurs?
# => This error occurs on line 193.
# 2. What is the type of error message?
# => This is a load error - occurs when a required file (rspec, extension script)
# fails to load.
# 3. What additional information does the interpreter provide about this type of error?
# => The interpreter states it cannot load such file because it does not exist 
# on my local repository. 
# 4. Where is the error in the code?
# => The error is that the use is telling the program to load a file that does
# not exist on my local machine or repository. The file "cartmand_essay.md" must
# be on my local machine or repo in order for this to work.
# 5. Why did the interpreter give you this error?
# => The interpreter gave me this error becuase it cannot locate this file
# that the program needs to compute this code. 


=begin 
--- REFLECTION -------------------------------------------------------
Which error was the most difficult to read?
I believe the first error we have to interpret ourselves was the hardest,
due to the fact that the line in which the error says it is on is tricky.
Usually it has more to do with a specific line of code, but it always looks
towards the end of the code, because that is where the end of output is located.

How did you figure out what the issue with the error was?
I was able to accurately read each error message and first go to the line in which
the error occurred. I'm familiar with errors enough to know basic problems
and why they are occurring - from there I was able to see what was going on and
why the error existed.

Were you able to determine why each error message happened based on the code? 
Yes, I have ran into all of these errors before and have practiced enough with 
error messages to recognize and understand each issue.

When you encounter errors in your future code, what process will you follow to help you debug?
I will go through each error and use the line in which the error occurs first -
if it is easy to point out with the description and error type labeled, I will
	go ahead and fix it. If it is more tricky like the first one - I will have to
	skim the code to see where I might of left out an end of output.
=end
