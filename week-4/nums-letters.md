What does puts do?
<code>puts</code> returns nil and prints out the output to the console with a new line.

What is an integer? What is a float?
An interger is a number without any decimals like 2, 5, 10, 7000. Whereas a float is any number with a decimal like 10.5 122.23098 ect. 

What is the difference between float and integer division? How would you explain the difference to someone who doesn't know anything about programming?
When two integers are divided by each other within a computer program, the return answer is also an integer. For example, if you had $20 and you wanted to go to a movie theater in Westchester, NY where a single movie is $12 a ticket - how many movies can you see? 20/12 = 1 movie ticket. That is how integers work in a computer program. If you wanted to know how much change you would have left over - you would divide floats 20.00/12.00 to receive 1 movie ticket with change of $8.00.

Section 2.5 Mini Challenges

```ruby
puts 24 * 365
puts (60 * 24) * 365 * 10
```
How does Ruby handle addition, subtraction, multiplication, and division of numbers?
Ruby computues these numbers based on the numbers given. For example, if I give ruby two integers to compute, the answer will be an integer. If I give ruby two floats, the answer will be a float.

What is the difference between integers and floats?
Integers are whole numbers without any decimals or fractions (2, 100, 5,000), whereas a float has decimals (9.89, 100.0926).

What is the difference between integer and float division?
An integer division will return an integer answer <code>puts 6 / 3</code> which will return 2. A float division will return a float answer <code>puts 9.0 / 2.0</code> will return 4.5.

What are strings? Why and when would you use them?
Strings are objects that represent a sequence of one or more characters. These are words used in the human language - strings help represent or manipulate words in a computer language. 

What are local variables? Why and when would you use them?
Local variables are variables that are set locally within a program beginning with a lowercase letter. They do not hold any arguements and can be set to a string or integer as long as its not set to another variable - things become more difficult. You would use a local variable so that you do not have to repeeat code - it stores code locally and we can reference that variable instead of re-typing it.

How was this challenge? Did you get a good review of some of the basics?
The challenge was a bit easy - however, it was good to review ruby and get back to the terminology used when studying for the interview process.

<p><a href="https://github.com/FJunior225/phase-0/blob/master/week-4/basic-math.rb">Basic Math</a></p>
<p><a href="https://github.com/FJunior225/phase-0/blob/master/week-4/defining-variables.rb">Defining Variables</a></p>
<p><a href="https://github.com/FJunior225/phase-0/blob/master/week-4/simple-string.rb">Simple String</a></p>