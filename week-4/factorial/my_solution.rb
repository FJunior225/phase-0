# Factorial

# I worked on this challenge Alyssa Ransbury.


# Your Solution Below

# define a method that takes in a parameter original_number
# add a counter to original_number
# counter = 1
# have counter add 1
# 
# until  number is less than or equal to original number
# 

def factorial(original_number)
 counter = 1
 fact_num = original_number

 if original_number == 0
   return 1
 end
 while counter < original_number
   fact_num = fact_num * (original_number - counter)
   counter += 1
 end

 return fact_num
end
