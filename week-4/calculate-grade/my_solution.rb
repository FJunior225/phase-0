# Calculate a Grade

# I worked on this challenge Maeve Tierney.


# Your Solution Below

def get_grade(avg_grade)
	if avg_grade >= 90
		return "A"
	elsif avg_grade >= 80
		return "B"
	elsif avg_grade >= 70
		return "C"
	elsif avg_grade >= 60
		return "D"
	else avg_grade < 60
		return "F"
	end
end
